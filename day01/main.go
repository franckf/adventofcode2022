package main

import (
	"fmt"
	"sort"

	"gitlab.com/franckf/adventofcode2022/utils"
)

func partOne() (result int) {
	input := utils.ReadInt()
	var byElf int
	for _, calories := range input {
		byElf = byElf + calories

		if calories == 0 {
			if result < byElf {
				result = byElf
			}
			byElf = 0
		}
	}

	return
}

func partTwo() (result int) {
	var elves []int
	input := utils.ReadInt()
	var byElf int
	for _, calories := range input {
		byElf = byElf + calories

		if calories == 0 {
			elves = append(elves, byElf)
			byElf = 0
		}
	}
	sort.Ints(elves)
	result = elves[len(elves)-1] + elves[len(elves)-2] + elves[len(elves)-3]
	return
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
