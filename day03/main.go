package main

import (
	"fmt"
	"strings"

	_ "embed"
)

//go:embed input.txt
var input string

func partOne() (result int) {
	/*
	   input := `vJrwpWtwJgWrhcsFMMfFFhFp
	   jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
	   PmmdzqPrVvPwwTWBwg
	   wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
	   ttgJtRGJQctTZtZT
	   CrZsJsPPZsGzwwsLwLmpwMDw`
	*/
	for _, rumstack := range strings.Split(input, "\n") {
		comp1 := rumstack[:len(rumstack)/2]
		comp2 := rumstack[len(rumstack)/2:]
		var both byte
		for i := 0; i < len(rumstack)/2; i++ {
			for j := 0; j < len(rumstack)/2; j++ {
				if comp1[i] == comp2[j] {
					both = comp1[i]
				}
			}
		}
		if rune(both) >= 96 {
			//	println(rune(both) - 96)
			result += int(rune(both) - 96)
		} else {
			//	println(rune(both) - 38)
			result += int(rune(both) - 38)
		}
	}
	return
}

func partTwo() (result int) {

	/*	input := `vJrwpWtwJgWrhcsFMMfFFhFp
		jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
		PmmdzqPrVvPwwTWBwg
		wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
		ttgJtRGJQctTZtZT
		CrZsJsPPZsGzwwsLwLmpwMDw`
	*/
	inputs := strings.Split(input, "\n")

	for i := 0; i < len(inputs); i += 3 {
		var trio byte
		var group []string
		group = append(group, inputs[i], inputs[i+1], inputs[i+2])

		for i := 0; i < len(group[0]); i++ {
			for j := 0; j < len(group[1]); j++ {
				for k := 0; k < len(group[2]); k++ {
					if group[0][i] == group[1][j] {
						if group[1][j] == group[2][k] {
							trio = group[2][k]
						}
					}
				}
			}
		}

		if rune(trio) >= 96 {
			result += int(rune(trio) - 96)
		} else {
			result += int(rune(trio) - 38)
		}
	}
	return
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
