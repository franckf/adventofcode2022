package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"

	_ "embed"
)

//go:embed input.txt
var input string

func partOne() (result int) {
	/*
	   	input := `2-4,6-8
	   2-3,4-5
	   5-7,7-9
	   2-8,3-7
	   6-6,4-6
	   2-6,4-8`
	*/
	assigns := strings.Fields(input)

	for _, pair := range assigns {
		firstElf := strings.Split(pair, ",")[0]
		seconElf := strings.Split(pair, ",")[1]
		rangefirstElf := strings.Split(firstElf, "-")
		rangeseconElf := strings.Split(seconElf, "-")
		sta1elf, _ := strconv.Atoi(rangefirstElf[0])
		end1elf, _ := strconv.Atoi(rangefirstElf[1])
		sta2elf, _ := strconv.Atoi(rangeseconElf[0])
		end2elf, _ := strconv.Atoi(rangeseconElf[1])
		var list1 []byte
		var list2 []byte
		for i := sta1elf; i <= end1elf; i++ {
			list1 = append(list1, byte(i))
		}
		for j := sta2elf; j <= end2elf; j++ {
			list2 = append(list2, byte(j))
		}
		if bytes.Contains(list1, list2) {
			result++
		} else if bytes.Contains(list2, list1) {
			result++
		}
	}

	return
}

func partTwo() (result int) {
	assigns := strings.Fields(input)
labelled:
	for _, pair := range assigns {
		firstElf := strings.Split(pair, ",")[0]
		seconElf := strings.Split(pair, ",")[1]
		rangefirstElf := strings.Split(firstElf, "-")
		rangeseconElf := strings.Split(seconElf, "-")
		sta1elf, _ := strconv.Atoi(rangefirstElf[0])
		end1elf, _ := strconv.Atoi(rangefirstElf[1])
		sta2elf, _ := strconv.Atoi(rangeseconElf[0])
		end2elf, _ := strconv.Atoi(rangeseconElf[1])

		for i := sta1elf; i <= end1elf; i++ {
			for j := sta2elf; j <= end2elf; j++ {
				if i == j {
					result++
					continue labelled
				}
			}
		}
	}

	return
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
