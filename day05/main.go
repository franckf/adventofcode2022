package main

import (
	"fmt"

	_ "embed"
)

//go:embed input.txt
var input string

func partOne() (result int) {

	input := `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`

	return
}

func partTwo() (result int) {

	return
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
