package main

import (
	"fmt"

	"gitlab.com/franckf/adventofcode2022/utils"
)

func partOne() (result int) {
	input := utils.ReadString()
	for _, v := range input {
		switch v {
		case "A X":
			result += 1 + 3
		case "B X":
			result += 1 + 0
		case "C X":
			result += 1 + 6
		case "A Y":
			result += 2 + 6
		case "B Y":
			result += 2 + 3
		case "C Y":
			result += 2 + 0
		case "A Z":
			result += 3 + 0
		case "B Z":
			result += 3 + 6
		case "C Z":
			result += 3 + 3
		}
	}
	return
}

func partTwo() (result int) {
	input := utils.ReadString()
	for _, v := range input {
		switch v {
		case "A X":
			result += 3 + 0
		case "B X":
			result += 1 + 0
		case "C X":
			result += 2 + 0
		case "A Y":
			result += 1 + 3
		case "B Y":
			result += 2 + 3
		case "C Y":
			result += 3 + 3
		case "A Z":
			result += 2 + 6
		case "B Z":
			result += 3 + 6
		case "C Z":
			result += 1 + 6
		}
	}
	return
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
